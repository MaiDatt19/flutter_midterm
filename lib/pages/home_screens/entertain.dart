import 'package:flutter/material.dart';

class EntertainScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return EntertainScreenState();
  }
}

class EntertainScreenState extends State<EntertainScreen> {
  final List<Map> listnews = <Map>[
    {
      'title': 'Nữ tình nguyện viên bật khóc khi chia tay đội U23 Malaysia',
      'src': 'Zing',
      'img':
          'https://znews-photo.zingcdn.me/w660/Uploaded/kbfhuo3/2022_05_17/AD0I6483.jpg'
    },
    {
      'title': 'Giới trẻ mong chờ gì ở thần tượng',
      'src': 'Zing',
      'img':
          'https://znews-photo.zingcdn.me/w660/Uploaded/mdf_drkydd/2022_05_17/273052407_510551690431395_7338626761015369932_n_1.jpg'
    },
    {
      'title': 'Quá khứ nghèo khó trước khi nổi tiếng của Kiều Minh Tuấn',
      'src': 'Trí thức & Cuộc sống',
      'img':
          'https://photo-cms-kienthuc.zadn.vn/zoom/800/uploaded/dinhcuc/2022_05_17/tuan/qua-khu-ngheo-kho-truoc-khi-noi-tieng-cua-kieu-minh-tuan-hinh-2.jpg'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
