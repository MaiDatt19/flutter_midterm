import 'package:flutter/material.dart';

class FoodScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FoodScreenState();
  }
}

class FoodScreenState extends State<FoodScreen> {
  final List<Map> listnews = <Map>[
    {
      'title':
          '5 niềm tự hào của ẩm thực Việt Nam từng được truyền thông quốc tế vinh danh',
      'src': 'VTC NEWS',
      'img':
          'https://baomoi-photo-fbcrawler.bmcdn.me/w720x480/2022_05_17_83_42623149/a15ab609434baa15f35a.jpg'
    },
    {
      'title': 'Thưởng thức những loại bánh mì sandwich ngon nhất thế giới',
      'src': 'Pháp luật VN',
      'img':
          'https://photo-baomoi.bmcdn.me/w700_r1/2022_05_17_523_42624076/976a4440b102585c0113.jpg'
    },
    {
      'title': 'Thực đơn Eat Clean giảm cân trong 7 ngày',
      'src': 'VietnamNet',
      'img':
          'https://static-images.vnncdn.net/files/publish/2022/5/10/thuc-don-giam-can-trong-7-ngay-ce6ab352d011471ba702058a236e8cf2.jpg'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
