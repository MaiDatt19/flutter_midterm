import 'package:flutter/material.dart';

class FollowedScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FollowedScreenState();
  }
}

class FollowedScreenState extends State<FollowedScreen> {
  final List<Map> listnews = <Map>[
    {
      'title': 'Yonhap: Triều Tiên điều máy bay tới Trung Quốc',
      'src': 'Zing',
      'img':
          'https://cdnimg.vietnamplus.vn/uploaded/fsmsy/2020_03_03/may_bay_do_tham.jpg'
    },
    {
      'title': '5 năm chờ đợi cho tấm HCV SEA Games',
      'src': 'Zing',
      'img':
          'https://znews-photo.zingcdn.me/w660/Uploaded/neg_etpyole/2022_05_17/Panh.jpg'
    },
    {
      'title': 'VĐV Philippines bị Nguyễn Thị Huyền vượt nhay trước vạch đích',
      'src': 'Zing',
      'img':
          'https://baomoi-photo-fbcrawler.bmcdn.me/w720x480/2022_05_17_119_42623224/d1b29ee06ba282fcdbb3.jpg'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
