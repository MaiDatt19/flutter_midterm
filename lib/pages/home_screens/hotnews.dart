import 'package:flutter/material.dart';
import 'package:timer_builder/timer_builder.dart';

class HotnewsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HotnewsScreenState();
  }
}

class HotnewsScreenState extends State<HotnewsScreen> {
  int swapRow = 1;
  final List<Map> listnews = <Map>[
    {
      'title':
          'Bài viết của Tổng Bí thư Nguyễn Phú trọng tiếp thêm niềm tin về con đường đi lên chủ nghĩa xã hội',
      'src': 'Nhân dân',
      'img':
          'http://quochoi.vn/content/tintuc/NewsMedia2020/Pham%20Bao%20Yen/Ky%20hop/04.01.22%20khai%20mac%20kh%20bat%20thuong%201.jpg'
    },
    {
      'title': 'Khởi tố tài xế dùng xe Mercedes tông chết người',
      'src': 'Zing',
      'img':
          'https://znews-photo.zingcdn.me/w660/Uploaded/nugzrd/2022_05_17/Bar_2_Dong.00_00_36_01.Still001.jpg'
    },
    {
      'title':
          'Quân đội Ukraine ra lệnh cho binh sĩ tại pháo đài Azovstal đầu hàng',
      'src': 'TienPhong',
      'img':
          'https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEi4RFio7GQfVoXA5jzw_Sxi43mPId3PQNu3Bu6ptmJxGVVvwwmfebYZa2MFFTSp100YSLAk9HrrLDBWCGzJZyCE9LTSgE7CaNxcF8eDzUQWGbumEl7Z2fWon03jCmEG56hAFVJYt-5ArAS6WQOQYJ1-OTcWgy03IJ93cnDCh16Ss6IiFpKlCzIAqpQc/s660/1652694695-40bbf824a838adeb1a2702b90503a414-width680height408.jpg'
    },
  ];
  // final List<Map> listnews = <Map>[
  //   {'title': '', 'src': '', 'img': ''},
  //   {'title': '', 'src': '', 'img': ''},
  //   {'title': '', 'src': '', 'img': ''}
  // ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(delegate: SliverChildListDelegate(<Widget>[buildSwapRow()])),
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }

  Widget buildSwapRow() {
    return Container(
      height: 50,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: [Color.fromARGB(255, 46, 221, 163), Colors.blue]),
      ),
      child: Row(
        children: [
          TimerBuilder.periodic(const Duration(seconds: 2), builder: (context) {
            return AnimatedSwitcher(
                duration: const Duration(seconds: 1),
                transitionBuilder: (Widget child, Animation<double> animation) {
                  return ScaleTransition(scale: animation, child: child);
                },
                child: updateRow());
          })
        ],
      ),
    );
  }

  Widget updateRow() {
    swapRow = swapRow == 1 ? 2 : 1;
    return swapRow == 1 ? localRow() : calendarRow();
  }

  Widget calendarRow() {
    return Container(
      padding: EdgeInsets.only(left: 15),
      child: Row(
        children: const [
          Icon(
            Icons.calendar_month_sharp,
            color: Color.fromARGB(255, 226, 107, 9),
          ),
          Text(
            ' T2, 9 tháng 5, 2022',
            style: TextStyle(color: Colors.white, fontSize: 17),
          )
        ],
      ),
    );
  }

  Widget localRow() {
    return Container(
      padding: EdgeInsets.only(left: 15),
      child: Row(
        children: const [
          Icon(
            Icons.cloud,
            color: Color.fromARGB(255, 213, 225, 228),
          ),
          Text(
            ' 28°C Bình Định',
            style: TextStyle(color: Colors.white, fontSize: 17),
          )
        ],
      ),
    );
  }

  // Widget buildListNews() {
  //   return Container(
  //     color: Colors.red,
  //     height: 30,
  //   );
  // }
}
