import 'package:flutter/material.dart';

class NewsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NewsScreenState();
  }
}

class NewsScreenState extends State<NewsScreen> {
  final List<Map> listnews = <Map>[
    {
      'title': 'Saigon Times CSR 2022 - Đầu tư cho những giá trị',
      'src': 'Saigon Times',
      'img':
          'https://thesaigontimes.vn/wp-content/uploads/2022/05/66-1920x1080-1.jpg'
    },
    {
      'title':
          'Giữ gìn nét đẹp văn hóa \'hầu đồng\' trước những biến tướng dị đoan',
      'src': 'Pháp luật VN',
      'img':
          'https://cand.com.vn/Files/Image/linhchi/2019/03/07/931b0cd6-e62e-47cf-a76e-a670a778df1e.jpg'
    },
    {
      'title':
          'Nguyễn Thị Huyền và Nguyễn Thị Oanh nhận bằng khen của Bộ trưởng',
      'src': 'Pháp Luật',
      'img':
          'https://photo-cms-baophapluat.zadn.vn/w800/Uploaded/2022/bcivvowk/2022_05_17/showimage-4-5974.jpg'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
