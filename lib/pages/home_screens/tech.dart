import 'package:flutter/material.dart';

class TechScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TechScreenState();
  }
}

class TechScreenState extends State<TechScreen> {
  final List<Map> listnews = <Map>[
    {
      'title':
          'Tỷ phú Elon Musk nêu điều kiện để tiếp tục thỏa thuận mua lại Twitter',
      'src': 'Tin tức',
      'img':
          'https://cdnimg.vietnamplus.vn/uploaded/bokttj/2022_05_17/ttxvn_elon_musk.jpg'
    },
    {
      'title': 'Mỹ chi 45 tỉ USD cho sáng kiến internet tham vọng',
      'src': 'Người lao động',
      'img':
          'https://nld.mediacdn.vn/zoom/700_438/291774122806476800/2022/5/16/interne-1652687152703825608021.png'
    },
    {
      'title': 'Google cho ra mắt đồng hồ thông minh đầu tiên',
      'src': 'Tin tức',
      'img':
          'https://cdn.thoibaotaichinhvietnam.vn/stores/news_dataimages/nguyenthanhha/052022/12/14/in_article/google-cho-ra-mat-dong-ho-thong-minh-dau-tien.jpg?rt=20220512143221'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
