import 'package:flutter/material.dart';

class LoveScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoveScreenState();
  }
}

class LoveScreenState extends State<LoveScreen> {
  final List<Map> listnews = <Map>[
    {
      'title': 'Ngày nay mọi người nghĩ gì về giới LGBT?',
      'src': 'Văn hóa phát triển',
      'img':
          'https://chongiadung.net/wp-content/uploads/2021/05/thinh_LGBT-la-gi-Tim-hieu-tat-ca-ve-LGBT-va-cong-dong-LGBT-2.jpg'
    },
    {
      'title': 'After 9PM số 14: Chỉ cần có mẹ',
      'src': 'SAOstar',
      'img':
          'https://ss-images.saostar.vn/w800/pc/1652768146767/saostar-i6eltz6k9vvu07su.jpg'
    },
    {
      'title':
          'Nghẹn ngào trước phản ứng của chú chó khi gặp lại chủ sau 1 tháng xa cách',
      'src': 'Gia đình',
      'img':
          'https://media.phapluatvacuocsong.vn/images/2022/05/10/21-1652168273-saostar-ruw7kqkwvy766o1p.gif'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
