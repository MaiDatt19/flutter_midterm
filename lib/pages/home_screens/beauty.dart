import 'package:flutter/material.dart';

class BeautyScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return BeautyScreenState();
  }
}

class BeautyScreenState extends State<BeautyScreen> {
  final List<Map> listnews = <Map>[
    {
      'title': 'Điều trị rối loạn giấc ngủ hậu Covid-19 bằng thảo mộc',
      'src': 'Zing',
      'img':
          'https://znews-photo.zingcdn.me/w660/Uploaded/sgorvz/2022_05_17/thao_moc_tri_mat_ngu_hau_Covid_19.jpg'
    },
    {
      'title': '7 mẹo ăn kiêng đơn giản để làn da khỏe mạnh vào mùa hè',
      'src': 'Quảng Ninh',
      'img':
          'https://media.baoquangninh.com.vn/upload/image/202205/medium/1970395_ee2b657d4649cce03f0e777332f8c193.jpg'
    },
    {
      'title': '5 thực phẩm cần tránh để có làn da mịn màng',
      'src': 'Người đưa tin',
      'img':
          'https://media1.nguoiduatin.vn/thumb_x992x595/media/vu-thu-huong/2022/05/16/thuc-pham-beo-final.jpg'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
