import 'package:flutter/material.dart';

class VNfootballScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return VNfootballScreenState();
  }
}

class VNfootballScreenState extends State<VNfootballScreen> {
  final List<Map> listnews = <Map>[
    {
      'title': 'U23 Việt Nam hứng khởi chờ đá bán kết với Malaysia',
      'src': 'An ninh Thủ đô',
      'img':
          'https://i.vietgiaitri.com/2022/5/17/u23-viet-nam-hung-khoi-cho-da-ban-ket-voi-malaysia-9c6-6451111.jpg'
    },
    {
      'title': 'ĐT futsal nữ Việt Nam chỉ cần hòa Thái Lan để giành HCV',
      'src': 'Doanh nghiệp VN',
      'img':
          'https://image.thanhnien.vn/w1024/Uploaded/2022/pwivoviu/2022_05_16/4-7531.jpg'
    },
    {
      'title':
          'Sân Việt Trì bảo đảm đủ tiêu chuẩn cho trận bán kết của U23 Việt Nam',
      'src': 'Tin tức',
      'img':
          'https://cdnmedia.baotintuc.vn/Upload/DMDnZyELa7xUDTdLsa19w/files/2022/05/1705/viet-tri-170622.jpeg'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
