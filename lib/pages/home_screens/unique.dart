import 'package:flutter/material.dart';

class UniqueScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return UniqueScreenState();
  }
}

class UniqueScreenState extends State<UniqueScreen> {
  final List<Map> listnews = <Map>[
    {
      'title': 'Lầu Năm Góc công bố video mật về UFO',
      'src': 'Zing',
      'img':
          'https://znews-photo.zingcdn.me/w660/Uploaded/jaegtn/2022_05_17/ezgif.com_gif_maker_18_.jpg'
    },
    {
      'title': 'Thế giới cũng phải thèm khát loài cá đắt đỏ nhất Việt Nam này',
      'src': 'Trí thức & Cuộc sống',
      'img':
          'https://photo-cms-kienthuc.zadn.vn/zoom/800/uploaded/thuydung/2022_05_17/6/1_AFDF.png'
    },
    {
      'title': 'Kích hoạt trí tưởng tượng của trí tuệ nhân tạo',
      'src': 'An ninh',
      'img':
          'https://img.cand.com.vn/resize/800x800/NewFiles/Images/2022/05/15/image001-1652589978183.jpg'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
