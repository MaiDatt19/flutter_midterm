import 'package:flutter/material.dart';

class TravelScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TravelScreenState();
  }
}

class TravelScreenState extends State<TravelScreen> {
  final List<Map> listnews = <Map>[
    {
      'title':
          'Đi đâu, ăn gì khi đến Quảng ninh cổ vũ tuyển nữ Việt Nam tại SEA Games 31?',
      'src': 'Giao thông',
      'img':
          'https://photo-baomoi.bmcdn.me/w720x480/2022_05_17_30_42624785/b289dcab29e9c0b799f8.jpg'
    },
    {
      'title': 'Quy Nhơn ngày mới',
      'src': 'Giáo dục VN',
      'img':
          'https://photo-cms-giaoduc.zadn.vn/Uploaded/2022/edxwpcqdh/2022_05_17/a2-7752.jpg'
    },
    {
      'title': 'Khám phá 4 hòn đảo thiên đường siêu đẹp tại Phú Quốc',
      'src': 'Pháp luật VN',
      'img':
          'https://phuquocxanh.com/vi/wp-content/uploads/2017/05/du-lich-phu-quoc-hon-mong-tay-4-1.jpg'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
