import 'package:flutter/material.dart';

class Car360Screen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return Car360ScreenState();
  }
}

class Car360ScreenState extends State<Car360Screen> {
  final List<Map> listnews = <Map>[
    {
      'title': '10 chiếc xe siêu độc được David Beckham đặc biệt yêu thích',
      'src': 'VietnamNet',
      'img':
          'https://photo-baomoi.bmcdn.me/w720x480/2022_05_17_23_42624418/38bfa79852dabb84e2cb.jpg'
    },
    {
      'title': 'Những mẫu xe hơi tiên phong thử nghiệm công nghệ an toàn',
      'src': 'XE giao thông',
      'img':
          'https://cdn.baogiaothong.vn/upload/images/2022-2/article_avatar_img/2022-05-17/img-bgt-2021-an-toan-phat-minh-6-1652729909-width497height280.jpg'
    },
    {
      'title': 'SUV đẹp long lanh, động cơ tăng áp, giá từ 356 triệu đồng',
      'src': 'Doanh nghiệp VN',
      'img':
          'https://photo-baomoi.bmcdn.me/w700_r1/2022_05_17_304_42625698/881264269164783a2175.jpg'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
