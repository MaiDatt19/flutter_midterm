import 'package:flutter/material.dart';

class LawScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LawScreenState();
  }
}

class LawScreenState extends State<LawScreen> {
  final List<Map> listnews = <Map>[
    {
      'title':
          'Chưa thu được tiền của 2 doanh nghiệp trúng đấu giá đất Thủ Thiêm',
      'src': 'Zing',
      'img':
          'https://znews-photo.zingcdn.me/w660/Uploaded/aohunkx/2021_03_23/TT_dat_thu_thiem.jpg'
    },
    {
      'title':
          'Triệt xóa điểm đá gà có người cảnh giới, bắt giữ hàng chục con bạc',
      'src': 'Người đưa tin',
      'img':
          'https://media1.nguoiduatin.vn/thumb_x992x595/media/bui-ngoc-diep/2022/05/17/da-ga-an-tien-1.jpg'
    },
    {
      'title':
          '\'Trốn\' thi hành án bằng cách mang bầu rồi vượt biên \'mất tích\' 10 năm',
      'src': 'Bảo vệ pháp luật',
      'img':
          'https://baovephapluat.vn/data/images/0/2022/05/17/hunghv/1.jpg?dpi=150&quality=100&w=820'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
