import 'package:flutter/material.dart';

class HealthScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HealthScreenState();
  }
}

class HealthScreenState extends State<HealthScreen> {
  final List<Map> listnews = <Map>[
    {
      'title':
          'Omicron khiến người mới khỏi Covid-19 vẫn có thể bị tái nhiễm nhiều lần',
      'src': 'VOV',
      'img':
          'https://media.vov.vn/sites/default/files/styles/large/public/2022-05/omicron_khien_nguoi_moi_khoi_covid-19_van_co_the_bi_tai_nhiem_nhieu_lan.jpg'
    },
    {
      'title': 'Một số tác dụng của việc uống cà phê đen',
      'src': 'Pháp luật VN',
      'img':
          'https://artcoffee.vn/wp-content/uploads/2020/09/8-loi-ich-to-lon-cua-viec-uong-ca-phe-den-nguyen-chat-khong-duong.jpg'
    },
    {
      'title': 'Công an TP Hà Nội khuyến cáo người dân đề phòng đuối nước',
      'src': 'Tin tức',
      'img':
          'https://cdnmedia.baotintuc.vn/Upload/Td3qmSNSjM5mhekL9vM2Q/files/2022/05/boi.jpeg'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
