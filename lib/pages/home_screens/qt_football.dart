import 'package:flutter/material.dart';

class QTfootballScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return QTfootballScreenState();
  }
}

class QTfootballScreenState extends State<QTfootballScreen> {
  final List<Map> listnews = <Map>[
    {
      'title': '7 người ảnh hưởng lớn nhất đến sự nghiệp của Son Heung-min',
      'src': 'Zing',
      'img':
          'https://znews-photo.zingcdn.me/w660/Uploaded/mdf_drokxr/2022_05_16/Son_6_2.jpg'
    },
    {
      'title': 'Arsenal trả giá vì không mua tiền đạo thay thế Aubameyang',
      'src': 'Doanh nghiệp VN',
      'img': 'https://cdn.bongdaplus.vn/Assets/Media/2022/05/17/70/arsenal.jpg'
    },
    {
      'title': 'Chelsea chính thức có suất dự Champions League nhờ... Arsenal',
      'src': 'Doanh nghiệp VN',
      'img':
          'https://cdn.bongdaplus.vn/Assets/Media/2022/05/17/77/chelsea-champions-league.jpg'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
