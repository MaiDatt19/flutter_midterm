import 'package:flutter/material.dart';

class WorldScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return WorldScreenState();
  }
}

class WorldScreenState extends State<WorldScreen> {
  final List<Map> listnews = <Map>[
    {
      'title': 'Ukraine tuyên bố ngưng mọi hòa đàm với Nga',
      'src': 'Zing',
      'img':
          'https://znews-photo.zingcdn.me/w660/Uploaded/jaegtn/2022_05_17/kharkiv_3.jpg'
    },
    {
      'title': 'Số người mất tích tại Mexico đã vượt mốc 100.000 người',
      'src': 'Vietnam+',
      'img':
          'https://cdnimg.vietnamplus.vn/t460/Uploaded/ivpycivo/2022_05_17/me1638680929431409834350.png'
    },
    {
      'title':
          'Tổng thống đắc cử Philippines đi nghỉ tại Australia sau chiến thắng',
      'src': 'Zing',
      'img':
          'https://znews-photo.zingcdn.me/w660/Uploaded/jaegtn/2022_05_17/marcos.jpg'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) {
            return Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 10),
                        //color: Colors.red,
                        child: Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(listnews[index]['img']))),
                        ),
                      ),
                      Row(
                        children: [Text(listnews[index]['src'])],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Wrap(
                        children: [
                          Text(
                            listnews[index]['title'],
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                    height: 10,
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(255, 211, 211, 211)))),
              ],
            );
          }, childCount: listnews.length),
        )
      ]),
    );
  }
}
