import 'package:flutter/material.dart';


class ContactScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return ContactScreenState();
  }

}

class ContactScreenState extends State<ContactScreen>{
  final string = 'Công ty Cổ phần Công nghệ EPI * Chịu trách nhiệm: Võ Quang \n Giấy phép số: 1818/GP-TTĐT do Sở Thông tin và Truyền thông Hà Nội cấp ngày 05/05/2017\n Địa chỉ: Tâng 5, Tòa nhà Báo Sinh Viên VN, D29 Phạm Văn Bạch, Yên Hòa, Cầu giấy, Hà Nội\n Tel: (024) 3-212-3232, số máy lẻ 6666\n Email: contact.baomoi@epi.com.vn';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Liên hệ'),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Color.fromARGB(255, 46, 221, 163), Color.fromARGB(31, 9, 35, 155)]
            ),
          ),
        ),
      ),
      body: Text(string, style: TextStyle(fontSize: 16),),
    );
  }
}