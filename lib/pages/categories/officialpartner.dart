import 'package:flutter/material.dart';


class OfficialPartnerScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return OfficialPartnerScreenState();
  }

}

class OfficialPartnerScreenState extends State<OfficialPartnerScreen>{
  final List<Map> partners = <Map>[
    {'bg':'https://baoquocte.vn/stores/site_data_data/administrator/122015/31/11/115447_thegioivietnam_logo_share.png'},
    {'bg':'https://onecms.1cdn.vn/2020/11/17/bao_bien_phong.png'},
    {'bg':'https://i1.wp.com/quangcaothangmay.vn/wp-content/uploads/2017/04/bang-gia-quang-cao-tren-bao-phap-luat-xa-hoi-2017.jpg?fit=800%2C400&ssl=1'},
    {'bg':'https://upload.wikimedia.org/wikipedia/vi/thumb/1/15/VietnamPlus.jpg/249px-VietnamPlus.jpg'},
    {'bg':'https://cdnstatic.baotintuc.vn/web_images/logo-share-02.jpg'},
    {'bg':'https://www.tuyengiao.vn/Skins/default/images/logo.png'},
    {'bg':'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTmdj_f7xr5tBUXS0-O1QIcK70OVVybkZhQfl9WbUWg72B9te028JGcnyqUzk-ppmppddk&usqp=CAU'},
    {'bg':'https://boxmedia.com.vn/wp-content/uploads/2018/12/bao-an-ninh-thu-do.jpg'},
    {'bg':'https://img.cand.com.vn/Content/images/logo.png'},
    {'bg':'https://upload.wikimedia.org/wikipedia/vi/d/d7/Logo-NhanDan.png'},
    {'bg':'https://static.kinhtedothi.vn/images/upload/2021/12/26/bao-kinh-te-dothi.jpg'},
    {'bg':'https://boxmedia.com.vn/wp-content/uploads/2019/03/nguoi-lao-dong-logo.jpg'},
    {'bg':'https://static-cms-sggp.zadn.vn/mobile/App_Themes/img/logo.png'},
    {'bg':'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyhLQ9HbMsy04fViHjCAXBJLkVV35ziK5ekA&usqp=CAU'},
    {'bg':'https://biendongmedia.com/upload/data/28_02_2018_1436823874_1332116861_1895034108.png'},
    {'bg':'https://upload.wikimedia.org/wikipedia/vi/2/22/Vietnamnet-Logo.png'},
    {'bg':'https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Logo_VOV.svg/2560px-Logo_VOV.svg.png'},
    {'bg':'https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/VTC_News_logo.svg/1200px-VTC_News_logo.svg.png'},
    {'bg':'https://kingcontent.lotuscdn.vn/crop/0,0_2000,2000/2019/9/19/logotoquoc-1568857846047554017369.png'},
    {'bg':'https://upload.wikimedia.org/wikipedia/commons/f/f0/Logo_b%C3%A1o_ph%C3%A1p_lu%E1%BA%ADt_Vi%E1%BB%87t_Nam.png'},
    {'bg':'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Emblem_of_Vietnam.svg/1200px-Emblem_of_Vietnam.svg.png'},
    {'bg':'https://congan.com.vn//Sites/CATP/css/images/bao-cong-an.png'},
    {'bg':'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQJ22Lkb_xoQ-_XG6Itj40ykV0nL5TkRxaDQPxAPpSC3Njm8_I3fwyAsXl0Bqf6mTb5EgI&usqp=CAU'},
    {'bg':'https://thoibaotaichinhvietnam.vn/stores/tpl_site_cfg_logo/hungdq.cms/102021/06/11/2457_logo600x600.png?rt=20211006112457'},
    {'bg':'https://cdnthuonghieu.muabannhanh.com/asset/home/img/500/saigon-times-online-thoi-bao-kinh-te-sai-gon-dua-tin-ve-muabannhanh-com-bang-phan-vai-moi-tren-thi-truong_5911542694d201494307878.png'},
    {'bg':'https://baoxaydung.com.vn/stores/tpl_site_cfg_logo/mastercms/042020/01/14/baoxaydungcom.png'},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Đối tác chính thức'),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Color.fromARGB(255, 46, 221, 163), Color.fromARGB(31, 9, 35, 155)]
            ),
          ),
        ),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          //color: Colors.grey,
          gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [Color.fromARGB(255, 255, 255, 255), Color.fromARGB(31, 199, 199, 199)]
            ),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                //padding: EdgeInsets.all(10),
                child: Text('ĐỐI TÁC CHÍNH THỨC', style: TextStyle(fontSize: 20, color: Color.fromARGB(255, 11, 170, 157)),)
              ),
              Divider(thickness: 1,),
              Container(
                height: 1060,
                padding: EdgeInsets.all(10),
                child: GridView.builder(
                  
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    childAspectRatio: 2.5, 
                  ),
                  itemCount: 26,
                  itemBuilder: (context, index){
                    return Card(
                      elevation: 3,  
                      child: Container(
                        decoration: BoxDecoration(
                          //color: Colors.red,
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            //colorFilter: ColorFilter.mode(Color.fromARGB(255, 102, 102, 102).withOpacity(1), BlendMode.darken,),
                            image: NetworkImage(partners[index]['bg'])
                          )
                        ),
                        child: Text('')
                      ),
                    );
                  }
                ),
              )
            ],
          ),
        ),
      )
    );
  }
}