import 'package:flutter/material.dart';


class SecurityScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return SecurityScreenState();
  }

}

class SecurityScreenState extends State<SecurityScreen>{
  final string01 = 'Thỏa thuận sử dụng và bảo mật này (sau đây gọi là “Thỏa thuận”) được lập ra bởi và giữa bạn (“bạn”) và Công ty Cổ phần công nghệ ePi (“ePi” hoặc “chúng tôi”) về việc sử dụng bất kỳ hay tất cả các loại dịch vụ (“Dịch vụ”) của Baomoi.com (“Baomoi”) của bạn. Bằng việc sử dụng Dịch vụ, bạn đồng ý chịu ràng buộc bởi những điều khoản và điều kiện này.';
  final string02 = 'Dịch vụ được thiết kế để giúp bạn tiếp cận thông tin tin tức được liên kết từ các trang không do ePi sở hữu hoặc quản lý. Cụ thể, Dịch vụ cung cấp những mô tả ngắn gọn về các bài báo để giúp bạn xác định nội dung chính của bài báo mà bạn quan tâm. Khi bạn lựa chọn một bài báo, bạn sẽ được kết nối tới website chứa bài báo đó (sau đây gọi là “website được kết nối”).';
  final string1 = 'Khi bạn đăng ký làm thành viên, bạn cần cung cấp cho chúng tôi một số thông tin cá nhân, ví dụ như họ tên, địa chỉ email và các thông tin khác do bạn cung cấp. Chúng tôi có thể kết hợp thông tin cá nhân do bạn cung cấp với các thông tin khác ngoài Dịch vụ hoặc từ các bên thứ ba để phân tích các nội dung mà bạn quan tâm.';
  final string2 = 'Mỗi khi bạn truy cập Dịch vụ, hệ thống của Baomoi sẽ sử dụng cookies và các kỹ thuật khác để lưu lại những hoạt động của bạn trên Baomoi. Server của Baomoi cũng sẽ tự động ghi lại thông tin khi bạn truy cập trang này và sử dụng các Dịch vụ bao gồm nhưng không giới hạn URL, địa chỉ IP, loại trình duyệt, ngôn ngữ, ngày giờ truy cập hoặc sử dụng Dịch vụ.';
  final string3 = 'Chúng tôi sử dụng thông tin cá nhân của bạn để nâng cao chất lượng của Dịch vụ, để gửi tới bạn các thông báo về dịch vụ mới của Baomoi hoặc dịch vụ của bên thứ ba mà chúng tôi tin rằng sẽ hữu ích với bạn.';
  final string4 = 'Chúng tôi có thể sử dụng thông tin cá nhân của bạn trong việc điều tra, nghiên cứu hoặc phân tích để vận hành và nâng cấp các kỹ thuật của Baomoi và Dịch vụ.';
  final string5 = 'Chúng tôi có thể sử dụng thông tin cá nhân của bạn để xác định xem liệu bạn có thể quan tâm đến các sản phẩm hay dịch vụ của bên thứ ba nào không.';
  final string6 = 'Về nguyên tắc, chúng tôi không cung cấp cho bên thứ ba thông tin cá nhân của bạn trừ các trường hợp sau:';
  final string7 = 'a. Bạn đồng ý để chúng tôi cung cấp thông tin cá nhân của bạn cho bên thứ ba; và/hoặc';
  final string8 = 'b. Chúng tôi cho rằng việc cung cấp thông tin cá nhân của bạn cho bên thứ ba là cần thiết, ví dụ nhằm tuân theo các yêu cầu pháp lý, ngăn chặn tội phạm hoặc bảo vệ an ninh quốc gia, hay bảo vệ an toàn cá nhân của những người sử dụng hoặc công chúng, v.v...; và/hoặc';
  final string9 = 'c. Bên thứ ba là đối tượng mua lại toàn bộ hay phần lớn pháp nhân sở hữu Baomoi và Dịch vụ; và/hoặc';
  final string10 = 'd. Thông tin cá nhân là thông tin vô danh về khách ghé thăm Baomoi. Chúng tôi có thể chia sẻ loại thông tin này cho bên thứ ba để họ có thể tìm hiểu về các loại khách tới thăm Baomoi và cách họ sử dụng Dịch vụ.';
  final string11 = 'Bạn có thể truy cập thông tin cá nhân của mình và sử đổi thông tin này nếu chúng chưa đúng hoặc xoá bỏ các thông tin đó. Việc thay đổi và/hoặc xoá bỏ như trên có thể không thực hiện được vào một số thời điểm nhất định khi có sự bất ổn định của hệ thống Baomoi. Trong trường hợp này, chúng tôi sẽ nỗ lực để bạn có thể tiếp tục sửa chữa hoặc xoá bỏ thông tin cá nhân sớm nhất có thể nhưng chúng tôi không chịu trách nhiệm về bất cứ vấn đề hoặc thiệt hại nào có thể có do việc chậm trễ này gây ra.';
  final string12 = 'Mật khẩu truy cập tài khoản của tất cả các thành viên của Baomoi đều được ePi bảo vệ. Tuy nhiên, an ninh mạng không an toàn tuyệt đối, vì thế ePi không chịu trách nhiệm về những thiệt hại có thể xảy ra.';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chính sách bảo mật'),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Color.fromARGB(255, 46, 221, 163), Color.fromARGB(31, 9, 35, 155)]
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(string01+'\n'+string02, style: TextStyle( fontSize: 16),),
            Text('Thỏa thuận bảo mật', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
            Text('1. Những thông tin mà chúng tôi thu thập.', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            Text(string1+'\n'+string2, style: TextStyle( fontSize: 16)),
            Text('2. Sử dụng thông tin.', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            Text(string3+'\n'+string4+'\n'+string5+'\n'+string6+'\n'+string7+'\n'+string8+'\n'+string9+'\n'+string10, style: TextStyle( fontSize: 16)),
            Text('3. Sửa đổi hoặc xóa thông tin.', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            Text(string11, style: TextStyle( fontSize: 16)),
            Text('4. Bảo mật.', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            Text(string12, style: TextStyle( fontSize: 16)),
          ],
        ),
      ),
    );
  }
}