import 'package:flutter/material.dart';


class UsageScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return UsageScreenState();
  }

}

class UsageScreenState extends State<UsageScreen>{
  final string1 = 'Thỏa thuận sử dụng và bảo mật này (sau đây gọi là “Thỏa thuận”) được lập ra bởi và giữa bạn (“bạn”) và Công ty Cổ phần công nghệ ePi (“ePi” hoặc “chúng tôi”) về việc sử dụng bất kỳ hay tất cả các loại dịch vụ (“Dịch vụ”) của Baomoi.com (“Baomoi”) của bạn. Bằng việc sử dụng Dịch vụ, bạn đồng ý chịu ràng buộc bởi những điều khoản và điều kiện này.';
  final string2 = 'Dịch vụ được thiết kế để giúp bạn tiếp cận thông tin tin tức được liên kết từ các trang không do ePi sở hữu hoặc quản lý. Cụ thể, Dịch vụ cung cấp những mô tả ngắn gọn về các bài báo để giúp bạn xác định nội dung chính của bài báo mà bạn quan tâm. Khi bạn lựa chọn một bài báo, bạn sẽ được kết nối tới website chứa bài báo đó (sau đây gọi là “website được kết nối”).';
  final string3 = 'ePi sở hữu và duy trì mọi quyền lợi sở hữu trí tuệ đối với Dịch vụ của mình nhưng ePi không tuyên bố là mình có quyền sở hữu trí tuệ đối với các bài báo tại các website được kết nối. Các quyền sở hữu trí tuệ này thuộc về các website được kết nối.';
  final string4 = 'ePi cũng không chịu trách nhiệm về các thông tin, dịch vụ và nội dung của những website được kết nối. Bạn là người chịu hoàn toàn trách nhiệm trong việc sử dụng, khai thác, cung cấp thông tin cá nhân, v.v... cho các website này.';
  final string5 = 'Bạn chỉ có thể sử dụng nội dung của Dịch vụ cho mục đích cá nhân của riêng bạn (cụ thể là không dùng cho mục đích thương mại) và không được sao chép, thay đổi, sửa đổi, tạo ra các tác phẩm phái sinh hoặc thể hiện một cách công khai bất kỳ nội dung nào của Dịch vụ. Chẳng hạn, bạn không được sử dụng Dịch vụ để bán một sản phẩm hay dịch vụ; hay sử dụng Dịch vụ để tăng số lượt truy cập tới trang web của bạn vì mục đích thương mại; hay sử dụng các kết quả từ Dịch vụ rồi định dạng lại và thể hiện công khai chúng, hay sử dụng các công cụ hoặc tài liệu khác để theo dõi hay sao chép bất kỳ nội dung nào từ Dịch vụ. Nếu bạn không chắc liệu dự định sử dụng Dịch vụ của bạn có được phép hay không, xin vui lòng liên hệ với chúng tôi.';
  final string6 = 'ePi sẽ không chịu trách nhiệm đối với việc gián đoạn, ngừng hoạt động, mất hoặc sai lệch dữ liệu từ những hành động tấn công của cá nhân hay tổ chức nào khác, cũng như về các sự cố kỹ thuật trong khi cung cấp Dịch vụ. ePi cũng không chịu trách nhiệm về các tổn thất phát sinh từ việc sử dụng Dịch vụ hoặc từ sự tương tác giữa bạn với những người dùng khác.';
  final string7 = 'ePi có quyền đơn phương đình chỉ hoặc chấm dứt Dịch vụ hoặc sự truy cập của bạn tới Dịch vụ.';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Điều khoản sử dụng'),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Color.fromARGB(255, 46, 221, 163), Color.fromARGB(31, 9, 35, 155)]
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(string1+'\n'+string2, style: TextStyle( fontSize: 16),),
            Text('Thỏa thuận sử dụng', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
            Text('1. Quyền gắn với Dịch vụ.', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            Text(string3+'\n'+string4, style: TextStyle( fontSize: 16)),
            Text('2. Sử dụng và chấm dứt sử dụng Dịch vụ.', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
            Text(string5+'\n'+string6+'\n'+string7, style: TextStyle( fontSize: 16)),
          ],
        ),
      )
    );
  }
}