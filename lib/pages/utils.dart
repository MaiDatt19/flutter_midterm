import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import 'profile.dart';

class UtilsScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return UtilsScreenState();
  }

}

class UtilsScreenState extends State<UtilsScreen>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('TIỆN ÍCH', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold ,color: Color.fromARGB(255, 102, 102, 102)),),
        actions: [
          CircleAvatar(
            backgroundColor: Color.fromARGB(255, 156, 156, 156),
            radius: 15,
            child: IconButton(
              padding: EdgeInsets.zero,
              onPressed: (){
                Navigator.push(context, 
                  PageTransition(
                    child: ProfileScreen(), 
                    type: PageTransitionType.rightToLeft
                  )
                );
              }, 
              icon: Icon(Icons.person, color: Colors.white,),
              
            ),
          ),
          SizedBox(width: 20,)
        ],
        flexibleSpace: Container(
          color: Colors.white,
        ),
        elevation: 0,
      ),
      body: ListView(
        children: <Widget>[
          buildXoso(),
          buildLichViet(),
          buildGiaVang(),
          buildThoiTiet(),
          buildVietlott()
        ],
      ),
    );
  }

  Widget buildXoso(){
    return Container(
      padding: const EdgeInsets.all(20),
      //color: Colors.red,
      height: 220,
      child: Column(
        children: [
          Row(
            children: [Text('XỔ SỐ', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.grey),)],
          ),
          SizedBox(height: 10,),
          Container(
            width: 500,
            height: 140,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage('https://cdn.discordapp.com/attachments/880745417170616365/970624034381389834/SmartSelect_20220502-165019_Bo_Mi1.jpg')
              )
            ),
          )
        ],
      ),
    );
  }
  Widget buildLichViet(){
    return Container(
      padding: const EdgeInsets.all(20),
      // color: Colors.yellow,
      height: 380,
      child: Column(
        children: [
          Row(
            children: [Text('LỊCH VIỆT', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.grey),)],
          ),
          SizedBox(height: 10,),
          Container(
            width: 500,
            height: 300,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage('https://cdn.discordapp.com/attachments/880745417170616365/970624067692535908/SmartSelect_20220502-165037_Bo_Mi1.jpg')
              )
            ),
          )
        ],
      ),
    );
  }
  Widget buildGiaVang(){
    return Container(
      padding: const EdgeInsets.all(20),
      //color: Colors.blue,
      height: 220,
      child: Column(
        children: [
          Row(
            children: [Text('GIÁ VÀNG', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.grey),)],
          ),
          SizedBox(height: 10,),
          Container(
            width: 500,
            height: 140,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage('https://cdn.discordapp.com/attachments/880745417170616365/970624118909190194/SmartSelect_20220502-165128_Bo_Mi1.jpg')
              )
            ),
          )
        ],
      ),
    );
  }
  Widget buildThoiTiet(){
    return Container(
      padding: const EdgeInsets.all(20),
      //color: Colors.pink,
      height: 330,
      child: Column(
        children: [
          Row(
            children: [Text('THỜI TIẾT', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.grey),)],
          ),
          SizedBox(height: 10,),
          Container(
            width: 500,
            height: 250,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage('https://cdn.discordapp.com/attachments/880745417170616365/970624095718891560/SmartSelect_20220502-165046_Bo_Mi1.jpg')
              )
            ),
          )
        ],
      ),
    );
  }
  Widget buildVietlott(){
    return Container(
      padding: const EdgeInsets.all(20),
      //color: Colors.orange,
      height: 230,
      child: Column(
        children: [
          Row(
            children: [Text('VIETLOTT', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.grey),)],
          ),
          SizedBox(height: 10,),
          Container(
            width: 500,
            height: 150,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage('https://cdn.discordapp.com/attachments/880745417170616365/970624143865315378/SmartSelect_20220502-165156_Bo_Mi1.jpg')
              )
            ),
          )
        ],
      ),
    );
  }
}