import 'package:flutter/material.dart';
import 'package:flutter_midterm/pages/categories/contact.dart';
import 'package:flutter_midterm/pages/categories/officialpartner.dart';
import 'package:flutter_midterm/pages/categories/security.dart';
import 'package:flutter_midterm/pages/categories/usage.dart';
import 'package:flutter_midterm/pages/home.dart';
import 'package:page_transition/page_transition.dart';

class CategoriesScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return CategoriesScreenState();
  }

}

class CategoriesScreenState extends State<CategoriesScreen>{
  final List<Map> categories = <Map>[
    {'text':'Theo dõi', 'bg':'https://likevn.com.vn/wp-content/uploads/2020/11/to-chuc-liveshow-ca-nhac-chuyen-nghiep.jpg'},
    {'text':'Nóng', 'bg':'https://baoquocte.vn/stores/news_dataimages/quangdao/042022/30/09/chelsea-sap-co-chu-moi-la-ty-phu-nguoi-my_1.jpg?rt=20220430092059'},
    {'text':'Mới', 'bg':'https://cdnimg.vietnamplus.vn/t620/uploaded/fsmsy/2016_11_19/cdv_vn.jpg'},
    {'text':'Bóng đá VN', 'bg':'https://cdn-img.thethao247.vn/resize_400x264/storage/files/huynguyen/2022/05/06/truc-tiep-u23-viet-nam-3-0-u23-indonesia-cuc-dien-an-bai-133983.jpg'},
    {'text':'Bóng đá QT', 'bg':'https://baoquocte.vn/stores/news_dataimages/quangdao/042022/30/09/chelsea-sap-co-chu-moi-la-ty-phu-nguoi-my_1.jpg?rt=20220430092059'},
    {'text':'Độc & Lạ', 'bg':'http://daihocthuyhanoi.edu.vn/assets/uploads/news/images/loi-ich-cua-viec-nuoi-thu-cung-02-300x200%402x.jpg'},
    {'text':'Tình yêu', 'bg':'https://tuvantamly.com.vn/wp-content/uploads/2016/02/s%E1%BB%B1-th%E1%BA%ADt-v%E1%BB%81-t%C3%ACnh-y%C3%AAu-2.jpg'},
    {'text':'Giải trí', 'bg':'https://e4life.vn/wp-content/uploads/2021/05/doan-hoi-thoai-tieng-anh-ve-am-nhac.jpg'},
    {'text':'Thế giới', 'bg':'https://tuyengiao.vn/Uploads/nguyenminhhue/world3ao.jpg'},
    {'text':'Pháp luật', 'bg':'https://quochoi.vn/content/tintuc/NewsMedia2020/Bao%20Yen/25.12.20%20bai%20toa%20an.jpg'},
    {'text':'Xe 360', 'bg':'https://cafefcdn.com/thumb_w/650/203337114487263232/2020/12/25/photo1608914559181-16089145600161677344320.jpg'},
    {'text':'Công Nghệ', 'bg':'https://kenhthongtintuyensinh.vn/wp-content/uploads/2018/12/c%C3%B4ng-ngh%E1%BB%87-trong-%C4%91%E1%BB%9Di-s%E1%BB%91ng.jpg'},
    {'text':'Ẩm thực', 'bg':'https://amthucvietnam365.vn/wp-content/uploads/2021/10/am-thuc-viet-nam-1.jpg'},
    {'text':'Làm đẹp', 'bg':'https://bloganchoi.com/wp-content/uploads/2021/07/cham-soc-da-1.jpg'},
    {'text':'Sức khỏe', 'bg':'http://hoangpr.vn/wp-content/uploads/2019/07/dich-vu-seo-web-suc-khoe-2.jpg'},
    {'text':'Du lịch', 'bg':'https://static.tapchitaichinh.vn/600x315/images/upload/duongthanhhai/05082020/15-dia-diem-du-lich-hot-nhat-viet-nam.jpg'},
  ];

  final List<Map> sources = <Map>[
    {'srcname':'VNNet', 'logo':'https://id.vietnamnet.vn/images/user-vietnamnet.png'},
    {'srcname':'Tiền Phong', 'logo':'https://cdnthuonghieu.muabannhanh.com/asset/home/img/500/562f177006116_1445926768.jpg'},
    {'srcname':'VTC News', 'logo':'http://image.vtc.vn/Content/images/logo-1.png'},
    {'srcname':'Zing', 'logo':'https://upload.wikimedia.org/wikipedia/vi/5/5e/Zing_official_logo.png'},
    {'srcname':'VOV', 'logo':'https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Logo_VOV.svg/2560px-Logo_VOV.svg.png'},
    {'srcname':'VNPlus', 'logo':'https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_170,w_170,f_auto,b_white,q_auto:eco,dpr_1/v1418963162/ne68hsh74makzblejov9.png'},
    {'srcname':'PLO', 'logo':'https://upload.wikimedia.org/wikipedia/commons/f/f0/Logo_b%C3%A1o_ph%C3%A1p_lu%E1%BA%ADt_Vi%E1%BB%87t_Nam.png'},
    {'srcname':'Infonet', 'logo':'https://bookingquangcao.com/public/userfiles/productsCat/infonet-vn.png'},
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Center(
          child: Text("Chuyên mục"),
        ),
        actions: [
          IconButton(
            onPressed: (){
              Navigator.pop(context);
            }, 
            icon: const Icon(Icons.arrow_forward)
          )
        ],
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Color.fromARGB(255, 46, 221, 163), Color.fromARGB(31, 9, 35, 155)]
            ),
          ),
        ),
      ),

      body: Container(
        color: const Color.fromARGB(255, 211, 211, 211),
        height: double.infinity,
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                constraints: BoxConstraints.tightFor(),
                padding: EdgeInsets.all(10),
                color: Colors.white,
                child: Column(
                  children: [
                    Row(children: [Text('CHỌN CHẾ ĐỘ ĐỌC', style: TextStyle(fontSize: 18, color: Colors.grey),)],),
                    Container(
                      padding: EdgeInsets.all(5),
                      height: 50,
                      //constraints: BoxConstraints.tightFor(),
                      child: GridView.count(
                        
                        childAspectRatio: 4.5,
                        crossAxisCount: 2,
                        crossAxisSpacing: 10,
                        //mainAxisSpacing: 10,
                      
                        children: [
                          ElevatedButton(
                            style: ButtonStyle(
                              elevation: MaterialStateProperty.all(0),
                              backgroundColor: MaterialStateProperty.all(Color.fromARGB(255, 230, 230, 230)),
                              foregroundColor: MaterialStateProperty.all(Colors.grey)
                            ),
                            onPressed: (){}, 
                            child: Text('Danh sách to', style: TextStyle(fontSize: 17),)
                          ),
                          ElevatedButton(
                            style: ButtonStyle(
                              elevation: MaterialStateProperty.all(0),
                              backgroundColor: MaterialStateProperty.all(Color.fromARGB(255, 230, 230, 230)),
                              foregroundColor: MaterialStateProperty.all(Colors.grey)
                            ),
                            onPressed: (){}, 
                            child: Text('Danh sách nhỏ', style: TextStyle(fontSize: 17),)
                          )
                        ]
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 10, child: Container(decoration: const BoxDecoration(color: Color.fromARGB(255, 211, 211, 211))),),
              buildGridViewCategories(),
              SizedBox(height: 10, child: Container(decoration: const BoxDecoration(color: Color.fromARGB(255, 211, 211, 211))),),
              buildSources(),
              SizedBox(height: 10, child: Container(decoration: const BoxDecoration(color: Color.fromARGB(255, 211, 211, 211))),),
              buildAboutUs()
            ],
          ),
        ),
      ),
    );
  }

  Widget buildGridViewCategories(){
    return Container(
      constraints: BoxConstraints.tightFor(),
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              Text('Thay đổi', style: TextStyle(fontSize: 18, color: Color.fromARGB(255, 11, 167, 91))),
              Spacer(),
              IconButton(
                onPressed: (){
                  Navigator.pop(context, 0);
                }, 
                icon: Icon(Icons.list, size: 30, color: Colors.grey,)
              )
            ],
          ),
          Container(
            //constraints: BoxConstraints.tightFor(),
            height: 1060,
            padding: EdgeInsets.all(10),
            child: GridView.builder(
              physics: NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                childAspectRatio: 1.5, 
              ),
              itemCount: 16,
              itemBuilder: (context, index){
                return InkWell(
                  child: AbsorbPointer(
                    child: Container(
                      decoration: BoxDecoration(
                        //color: Colors.red,
                        borderRadius: BorderRadius.circular(10),
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          colorFilter: ColorFilter.mode(Color.fromARGB(255, 102, 102, 102).withOpacity(1), BlendMode.darken,),
                          image: NetworkImage(categories[index]['bg'])
                        )
                      ),
                      child: TextButton(
                        onPressed: (){},
                        child: Text(categories[index]['text'], style: TextStyle(fontSize: 20, color: Colors.white),)
                      ),
                    ),
                  ),
                  onTap: (){
                    Navigator.pop(context, index);
                  },
                );
              }
            ),
          )
        ],
      ),
    );
  }
  Widget buildSources(){
    return Container(
      constraints: BoxConstraints.tightFor(),
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Column(
        children: [
          Row(children: [Text('NGUỒN BÁO NỔI BẬT', style: TextStyle(fontSize: 18, color: Colors.grey),)],),
          Container(
            //color: Colors.yellow,
            padding: const EdgeInsets.only(top: 15),
            height: 230,
            child: GridView.builder(
              physics: NeverScrollableScrollPhysics(),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                childAspectRatio: 0.9, 
              ),
              itemCount: 8,
              itemBuilder: (context, index){
                return GestureDetector(
                  child: Container(
                    //color: Colors.red,
                    child: Column(
                      children: [
                        CircleAvatar(
                          radius: 25,
                          backgroundImage: NetworkImage(sources[index]['logo']),
                        ),
                        TextButton(
                          onPressed: (){}, 
                          child: Text(sources[index]['srcname'], style: TextStyle(color: Colors.black),)
                        )
                      ],
                    ),
                  ),
                  onTap:(){

                  }
                );
              }
            ),
          ),
          //Spacer(),
          Divider(),
          Center(
            child: Text("Xem tất cả"),
          )
        ],
      ),
    );
  }
  Widget buildAboutUs(){
    return Container(
      constraints: BoxConstraints.tightFor(),
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Column(
        children: [
          GestureDetector(
            child: Row(
              children: [
                TextButton(
                  onPressed: (){
                    Navigator.push(context, 
                      PageTransition(
                        child: ContactScreen(), 
                        type: PageTransitionType.rightToLeft
                      )
                    );
                  }, 
                  child: Text('Liên hệ', style: TextStyle(fontSize: 17, color: Colors.black),)
                )
              ],
            ),
          ),
          GestureDetector(
            child: Row(
              children: [
                TextButton(
                  onPressed: (){
                    Navigator.push(context, 
                      PageTransition(
                        child: OfficialPartnerScreen(), 
                        type: PageTransitionType.rightToLeft
                      )
                    );
                  }, 
                  child: Text('Đối tác chính thức', style: TextStyle(fontSize: 17, color: Colors.black),)
                )
              ],
            ),
          ),
          GestureDetector(
            child: Row(
              children: [
                TextButton(
                  onPressed: (){
                    Navigator.push(context, 
                      PageTransition(
                        child: UsageScreen(), 
                        type: PageTransitionType.rightToLeft
                      )
                    );
                  }, 
                  child: Text('Điều khoản sử dụng', style: TextStyle(fontSize: 17, color: Colors.black),)
                )
              ],
            ),
          ),
          GestureDetector(
            child: Row(
            children: [
              TextButton(
                onPressed: (){
                  Navigator.push(context, 
                    PageTransition(
                      child: SecurityScreen(), 
                      type: PageTransitionType.rightToLeft
                    )
                  );
                }, 
                child: Text('Chính sách bảo mật', style: TextStyle(fontSize: 17, color: Colors.black),)
              )
            ],
          ),
          ),
        ],
      )
    );
  }
}