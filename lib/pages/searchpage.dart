import 'package:flutter/material.dart';

class SearchpageScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return SearchpageScreenState();
  }

}

class SearchpageScreenState extends State<SearchpageScreen>{
  var searchController = TextEditingController();
  final List<Map> hot24h_news = <Map>[
    {"img":'https://afamilycdn.com/zoom/640_400/pM9qX6IKLO17GoceCpxHn4vfNhcccc/Image/2012/08/120830afamilyTS3avaatr-5a7f7.jpg', 'title':'Bạo lực tinh thần và những nỗi đau vô hình', 'src':'VOV', 'time':'5 ngày'},
    {"img":'https://znews-photo.zingcdn.me/w660/Uploaded/gtntnn/2022_04_25/CPDC4OEGJ5KS3MA5QBSDUNYFPU.jpeg', 'title':'Khó khăn đợi chờ ông Macron trong nhiệm kỳ 2', 'src':'Zing', 'time':'5 ngày'},
    {"img":'https://cdnmedia.baotintuc.vn/Upload/3qVxwVtNEPp6Wp9kkF77g/files/2022/04/25/imf-250422.jpg', 'title':'Cuộc khủng hoảng đa tầng', 'src':'tin tức', 'time':'6 ngày'}
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Container(
          decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.2),
            borderRadius: BorderRadius.circular(10),
          ),
            width: double.infinity,
            height: 40,
            child: TextField(
            style: TextStyle(color: Colors.white),
            cursorColor: Colors.white,
            controller: searchController,
            decoration: InputDecoration(
              fillColor: Colors.white,
              border: InputBorder.none,
              iconColor: Colors.white,
              hintStyle: TextStyle(color: Colors.white),
              prefixIcon: Icon(Icons.search, color: Colors.white,),
              suffixIcon: IconButton(
                onPressed:(){
                  searchController.text = "";
                }, 
                icon: Icon(Icons.clear, color: Colors.white,),
              ),
              hintText: "Tìm kiếm..."
            ),
          ),
          ),
        actions: [
          
          TextButton(
            onPressed: (){
              Navigator.pop(context);
            }, 
            child: Text('ĐÓNG', style: TextStyle(fontSize: 18, color: Colors.white),),
          )
        ],
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Color.fromARGB(255, 46, 221, 163), Color.fromARGB(31, 9, 35, 155)]
            ),
          ),
          
        ),
      ),
      body: Container(
        color: const Color.fromARGB(255, 211, 211, 211),
        height: double.infinity,
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
          children: [
            Container(
              constraints: BoxConstraints.tightFor(),
              color: Colors.white,
              padding: EdgeInsets.all(20),
              // height: 200,
              child: Column(
                children: [
                  Row(
                    children: [Text('TÌM NHANH', style: TextStyle(fontSize: 20, color: Colors.grey),)],
                  ),
                  SizedBox(height: 10,),
                  Wrap(
                    spacing: 10,
                    runSpacing: 10,
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(width: 2, color: Color.fromARGB(255, 207, 207, 207)),
                          borderRadius: BorderRadius.circular(5)
                        ),
                        constraints: BoxConstraints.tightFor(),
                        child: Text('Liên minh châu âu', style: TextStyle(color: Colors.grey),),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(width: 2, color: Color.fromARGB(255, 207, 207, 207)),
                          borderRadius: BorderRadius.circular(5)
                        ),
                        constraints: BoxConstraints.tightFor(),
                        child: Text('Mai Dat', style: TextStyle(color: Colors.grey),),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(width: 2, color: Color.fromARGB(255, 207, 207, 207)),
                          borderRadius: BorderRadius.circular(5)
                        ),
                        constraints: BoxConstraints.tightFor(),
                        child: Text('Yasuo', style: TextStyle(color: Colors.grey),),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(width: 2, color: Color.fromARGB(255, 207, 207, 207)),
                          borderRadius: BorderRadius.circular(5)
                        ),
                        constraints: BoxConstraints.tightFor(),
                        child: Text('Peaky Blinders', style: TextStyle(color: Colors.grey),),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(width: 2, color: Color.fromARGB(255, 207, 207, 207)),
                          borderRadius: BorderRadius.circular(5)
                        ),
                        constraints: BoxConstraints.tightFor(),
                        child: Text('Đàm Vĩnh Hưng', style: TextStyle(color: Colors.grey),),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          border: Border.all(width: 2, color: Color.fromARGB(255, 207, 207, 207)),
                          borderRadius: BorderRadius.circular(5)
                        ),
                        constraints: BoxConstraints.tightFor(),
                        child: Text('Liverpool', style: TextStyle(color: Colors.grey),),
                      ),
                    ],
                  ),
                ],
              ),
              //color: Colors.red,
            ),
            SizedBox(height: 10,child: Container(decoration: const BoxDecoration(color: Color.fromARGB(255, 211, 211, 211)))),
            Container(
              color: Colors.white,
              constraints: BoxConstraints.tightFor(),
              padding: EdgeInsets.all(20),
              height: 400,
              child: Column(
                children: [
                  Row(
                    children: [
                      Text('NÓNG 24H', style: TextStyle(fontSize: 20, color: Colors.grey),),
                    ],
                  ),
                  ListView.separated(
                    shrinkWrap: true,
                    itemCount: hot24h_news.length,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index){
                      return ListTile(
                        leading: Container(
                          width: 70,
                          height: 80,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage("${hot24h_news[index]['img']}")
                            )
                          ),
                        ),
                        title: Text(hot24h_news[index]['title'], style: TextStyle(fontSize: 20),),
                        subtitle: Text('\n'+hot24h_news[index]['src'] +'    '+ hot24h_news[index]['time']),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) => const Divider(),
                  ),
                  
                  Spacer(),
                  Divider(),
                  Center(
                    child: Text("Đọc thêm"),
                  )
                ],
              ),
              //color: Colors.red,
            ),
          ],
        ),
        )
      ),
    );
  }
}