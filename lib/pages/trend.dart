import 'package:flutter/material.dart';

class TrendScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return TrendScreenState();
  }

}

class TrendScreenState extends State<TrendScreen>{
  double container_height = 450;
  final List<Map> trend1_news = <Map>[
    {"img":'https://nld.mediacdn.vn/291774122806476800/2022/5/1/279396252965255384189518295123802999703237n-16513110713721931758748-1651311177812100398644-1651399600080564687850.jpg', 'title':'11 người tử vong do tai nạn giao thông trong ngày 1/5', 'src':'Vietnamnet', 'time':'3 giờ'},
    {"img":'https://photo-baomoi.bmcdn.me/w700_r16x9_sm/2022_05_01_94_42475480/4757d8f827bacee497ab.jpg', 'title':'Ngày 1/5: Chỉ còn 3.717 ca COVID-19, giảm xuống thấp nhất trong khoảng 270 ngày qua', 'src':'SỨCKHỎE-ĐỜISỐNG', 'time':'1 giờ'},
    {"img":'https://nld.mediacdn.vn/291774122806476800/2022/3/25/hang2-1648185792223288116552.png', 'title':'Diễn biến vụ Nguyễn Phương Hằng bị bà Đặng Thị Hàn Ni tố cáo', 'src':'NGƯỜI LAO ĐỘNG', 'time':'3 giờ'}
  ];
  final List<Map> trend2_news = <Map>[
    {"img":'https://img.cand.com.vn/NewFiles/Images/2022/04/30/4-1651328581717.jpg', 'title':'\'Bùng nổ\' sự kiện kỳ nghỉ \'vàng\': Các địa phương \'siết\' nạn chặt chém', 'src':'Vietnam+', 'time':'8 giờ'},
    {"img":'http://media.tinthethao.com.vn/files/bongda/2022/04/28/gettyimages-977341848-1903jpg.jpg', 'title':'Mino Raiola qua đời ở tuổi 54', 'src':'Zing', 'time':'21 giờ'},
    {"img":'https://cdnmedia.baotintuc.vn/Upload/of1YDQmgYWjUVVEP2wPLg/files/2020/06/Mua/CHI_0192.JPG', 'title':'Thời tiết ngày 1-5: Mưa lớn tài nhiều vùng, miền trong cả nước', 'src':'Quân đội nhân dân', 'time':'12 giờ'}
  ];
  final List<Map> trend3_news = <Map>[
    {"img":'https://afamilycdn.com/zoom/640_400/pM9qX6IKLO17GoceCpxHn4vfNhcccc/Image/2012/08/120830afamilyTS3avaatr-5a7f7.jpg', 'title':'Bạo lực tinh thần và những nỗi đau vô hình', 'src':'VOV', 'time':'5 ngày'},
    {"img":'https://znews-photo.zingcdn.me/w660/Uploaded/gtntnn/2022_04_25/CPDC4OEGJ5KS3MA5QBSDUNYFPU.jpeg', 'title':'Khó khăn đợi chờ ông Macron trong nhiệm kỳ 2', 'src':'Zing', 'time':'5 ngày'},
    {"img":'https://cdnmedia.baotintuc.vn/Upload/3qVxwVtNEPp6Wp9kkF77g/files/2022/04/25/imf-250422.jpg', 'title':'Cuộc khủng hoảng đa tầng', 'src':'tin tức', 'time':'6 ngày'}
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        
        title: Center(
          child: Text("Xu hướng"),
        ) ,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Color.fromARGB(255, 46, 221, 163), Color.fromARGB(31, 9, 35, 155)]
            ),
          ),
        ),
      ),
      body: ListView(
              children: [
                buildTrend1(),
                SizedBox(height: 10, child: Container(decoration: const BoxDecoration(color: Color.fromARGB(255, 211, 211, 211))),),
                buildTrend2(),
                SizedBox(height: 10,child: Container(decoration: const BoxDecoration(color: Color.fromARGB(255, 211, 211, 211))),),
                buildTrend3()
              ],
            )
      );
  }

  Widget buildTrend1(){
    return Container(
      padding: const EdgeInsets.all(15),
      height: container_height,
      // color: Colors.red,
      child: Column(
        children: [
          Row(
            children: [
              Icon(Icons.trending_up, color: Color.fromARGB(255, 11, 167, 91), size: 25,),
              Text(' ĐANG ĐƯỢC QUAN TÂM', style: TextStyle(fontSize: 18, color: Color.fromARGB(255, 11, 167, 91)),)
            ],
          ),
          ListView.separated(
            shrinkWrap: true,
            itemCount: trend1_news.length,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int index){
              return ListTile(
                leading: Container(
                  width: 70,
                  height: 80,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage("${trend1_news[index]['img']}")
                    )
                  ),
                ),
                title: Text(trend1_news[index]['title'], style: TextStyle(fontSize: 20),),
                subtitle: Text('\n'+trend1_news[index]['src'] +'    '+ trend1_news[index]['time']),
              );
            },
            separatorBuilder: (BuildContext context, int index) => const Divider(),
          ),
          Spacer(),
          Divider(),
          Center(
            child: Text("Đọc thêm"),
          )
        ],
      ),
    );
  }

  Widget buildTrend2(){
    return Container(
      padding: const EdgeInsets.all(15),
      height: container_height,
      //color: Colors.yellow,
      child: Column(
        children: [
          Row(
            children: [
              Icon(Icons.trending_up, color: Color.fromARGB(255, 11, 167, 91), size: 25,),
              Text(' NÓNG 24H', style: TextStyle(fontSize: 18, color: Color.fromARGB(255, 11, 167, 91)),)
            ],
          ),
          ListView.separated(
            shrinkWrap: true,
            itemCount: trend2_news.length,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int index){
              return ListTile(
                leading: Container(
                  width: 70,
                  height: 80,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage("${trend2_news[index]['img']}")
                    )
                  ),
                ),
                title: Text(trend2_news[index]['title'], style: TextStyle(fontSize: 20),),
                subtitle: Text('\n'+trend2_news[index]['src'] +'    '+ trend2_news[index]['time']),
              );
            },
            separatorBuilder: (BuildContext context, int index) => const Divider(),
          ),
          Spacer(),
          Divider(),
          Center(
            child: Text("Đọc thêm"),
          )
        ],
      ),
    );
  }

  Widget buildTrend3(){
    return Container(
      padding: const EdgeInsets.all(15),
      height: container_height,
      //color: Colors.green,
      child: Column(
        children: [
          Row(
            children: [
              Icon(Icons.trending_up, color: Color.fromARGB(255, 11, 167, 91), size: 25,),
              Text(' GÓC NHÌN VÀ PHÂN TÍCH', style: TextStyle(fontSize: 18, color: Color.fromARGB(255, 11, 167, 91)),)
            ],
          ),
          ListView.separated(
            shrinkWrap: true,
            itemCount: trend3_news.length,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, int index){
              return ListTile(
                leading: Container(
                  width: 70,
                  height: 80,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage("${trend3_news[index]['img']}")
                    )
                  ),
                ),
                title: Text(trend3_news[index]['title'], style: TextStyle(fontSize: 20),),
                subtitle: Text('\n'+trend3_news[index]['src'] +'    '+ trend3_news[index]['time']),
              );
            },
            separatorBuilder: (BuildContext context, int index) => const Divider(),
          ),
          Spacer(),
          Divider(),
          Center(
            child: Text("Đọc thêm"),
          )
        ],
      ),
    );
  }
}