import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_midterm/pages/categories.dart';
import 'package:flutter_midterm/pages/home_screens/beauty.dart';
import 'package:flutter_midterm/pages/home_screens/car360.dart';
import 'package:flutter_midterm/pages/home_screens/entertain.dart';
import 'package:flutter_midterm/pages/home_screens/followed.dart';
import 'package:flutter_midterm/pages/home_screens/food.dart';
import 'package:flutter_midterm/pages/home_screens/health.dart';
import 'package:flutter_midterm/pages/home_screens/hotnews.dart';
import 'package:flutter_midterm/pages/home_screens/law.dart';
import 'package:flutter_midterm/pages/home_screens/love.dart';
import 'package:flutter_midterm/pages/home_screens/news.dart';
import 'package:flutter_midterm/pages/home_screens/qt_football.dart';
import 'package:flutter_midterm/pages/home_screens/tech.dart';
import 'package:flutter_midterm/pages/home_screens/travel.dart';
import 'package:flutter_midterm/pages/home_screens/unique.dart';
import 'package:flutter_midterm/pages/home_screens/vn_football.dart';
import 'package:flutter_midterm/pages/home_screens/world.dart';
import 'package:flutter_midterm/pages/profile.dart';
import 'package:flutter_midterm/pages/searchpage.dart';
import 'package:page_transition/page_transition.dart';

class HomeScreen extends StatefulWidget {
  final home_index;

  const HomeScreen({Key? key, required this.home_index}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  var home_index = 1;
  bool pressed = true;
  final List<String> title_list = <String>[
    "Theo dõi",
    "Nóng",
    "Tin mới",
    "Bóng đá VN"
  ];
  final List<Map> data = <Map>[
    {'title': 'Theo dõi', 'isSelected': false},
    {'title': 'Nóng', 'isSelected': true},
    {'title': 'Tin mới', 'isSelected': false},
    {'title': 'Bóng đá VN', 'isSelected': false},
    {'title': 'Bóng đá QT', 'isSelected': false},
    {'title': 'Độc & Lạ', 'isSelected': false},
    {'title': 'Tình yêu', 'isSelected': false},
    {'title': 'Giải trí', 'isSelected': false},
    {'title': 'Thế giới', 'isSelected': false},
    {'title': 'Pháp luật', 'isSelected': false},
    {'title': 'Xe 360', 'isSelected': false},
    {'title': 'Công Nghệ', 'isSelected': false},
    {'title': 'Ẩm thực', 'isSelected': false},
    {'title': 'Làm đẹp', 'isSelected': false},
    {'title': 'Sức khỏe', 'isSelected': false},
    {'title': 'Du lịch', 'isSelected': false},
  ];
  final home_screens = [
    FollowedScreen(),
    HotnewsScreen(),
    NewsScreen(),
    VNfootballScreen(),
    QTfootballScreen(),
    UniqueScreen(),
    LoveScreen(),
    EntertainScreen(),
    WorldScreen(),
    LawScreen(),
    Car360Screen(),
    TechScreen(),
    FoodScreen(),
    BeautyScreen(),
    HealthScreen(),
    TravelScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: buildHomeScreen()
        // SingleChildScrollView(
        //   child: Column(
        //     children: [

        //     ],
        //   ),
        // )

        );
  }

  Widget buildHomeScreen() {
    return Scaffold(
      appBar: AppBar(
        leading:
            IconButton(onPressed: moveToCategory, icon: const Icon(Icons.menu)),
        actions: <Widget>[
          SizedBox(
              width: 270,
              child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return TextButton(
                                onPressed: () {
                                  setState(() {
                                    if (data[index]['isSelected'] == false) {
                                      data[index]['isSelected'] =
                                          !data[index]['isSelected'];
                                      for (var i = 0; i < data.length; i++) {
                                        if (i != index) {
                                          data[i]['isSelected'] = false;
                                        }
                                      }
                                      home_index = index;
                                    }
                                  });
                                },
                                child: Text(data[index]['title'],
                                    style: data[index]['isSelected'] == false
                                        ? TextStyle(
                                            color: Color.fromARGB(
                                                255, 218, 217, 217))
                                        : TextStyle(
                                            color: Colors.white,
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold)));
                          }),
                    ],
                  ))),
          IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    PageTransition(
                        child: SearchpageScreen(),
                        type: PageTransitionType.rightToLeft));
              },
              tooltip: "Tìm kiếm",
              icon: const Icon(Icons.search)),
          IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    PageTransition(
                        child: ProfileScreen(),
                        type: PageTransitionType.rightToLeft));
              },
              tooltip: "Trang cá nhân",
              icon: const Icon(Icons.person))
        ],
        elevation: 0,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Color.fromARGB(255, 46, 221, 163),
                  Color.fromARGB(31, 9, 35, 155)
                ]),
          ),
        ),
      ),
      body: IndexedStack(
        index: home_index,
        children: home_screens,
      ),
    );
  }

  void setSelected(int index) {
    setState(() {
      if (data[index]['isSelected'] == false) {
        data[index]['isSelected'] = !data[index]['isSelected'];
        for (var i = 0; i < data.length; i++) {
          if (i != index) {
            data[i]['isSelected'] = false;
          }
        }
        home_index = index;
      }
    });
  }

  void moveToCategory() async {
    final home_indexs = await Navigator.push(
        context,
        PageTransition(
            child: CategoriesScreen(), type: PageTransitionType.leftToRight));
    var new_index = int.parse('$home_indexs');
    //log('msg$home_indexs');
    setSelected(new_index);
  }
}
