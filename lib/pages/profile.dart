import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProfileScreenState();
  }
}

class ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
              iconTheme: IconThemeData(color: Colors.black),
              backgroundColor: Colors.white,
              pinned: true,
              snap: true,
              floating: true,
              expandedHeight: 150,
              title: Text(
                'User',
                style: TextStyle(color: Colors.black),
              ),
              //centerTitle: true,
              flexibleSpace: FlexibleSpaceBar(
                background: Container(
                  height: 150,
                  width: double.infinity,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(150),
                        bottomRight: Radius.circular(150)),
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [
                          Color.fromARGB(255, 46, 221, 163),
                          Color.fromARGB(171, 11, 34, 138)
                        ]),
                  ),
                  padding: EdgeInsets.all(40),
                  child: CircleAvatar(
                      radius: 50,
                      child: ClipOval(
                        child: Image.asset('images/qa.png'),
                      )),
                ),
              )),
          SliverList(
              delegate: SliverChildListDelegate(<Widget>[
            buildTienIch(),
            SizedBox(
                height: 10,
                child: Container(
                    decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 211, 211, 211)))),
            buildCaiDat(),
            SizedBox(
                height: 10,
                child: Container(
                    decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 211, 211, 211)))),
            buildSanPham(),
            SizedBox(
                height: 10,
                child: Container(
                    decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 211, 211, 211)))),
            buildLogOut()
          ]))
        ],
      ),
    );
  }

  Widget buildTienIch() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Text(
                'TIỆN ÍCH',
                style: TextStyle(
                    color: Color.fromARGB(255, 11, 167, 91), fontSize: 18),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Icon(
                Icons.calendar_today,
                color: Colors.grey,
              ),
              Text(
                ' Lịch Việt',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Icon(
                Icons.sunny,
                color: Colors.grey,
              ),
              Text(
                ' Thời tiết',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Icon(
                Icons.money,
                color: Colors.grey,
              ),
              Text(
                ' Kết quả xổ số',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Icon(
                Icons.price_check,
                color: Colors.grey,
              ),
              Text(
                ' Giá vàng & Ngoại tệ',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Icon(
                Icons.sports,
                color: Colors.grey,
              ),
              Text(
                ' Live Score',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Icon(
                Icons.wifi,
                color: Colors.grey,
              ),
              Text(
                ' Tiết kiệm 3G/4G truy cập',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Icon(
                Icons.more,
                color: Colors.grey,
              ),
              Text(
                ' Tiện ích khác',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget buildCaiDat() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Text(
                'CÀI ĐẶT',
                style: TextStyle(
                    color: Color.fromARGB(255, 11, 167, 91), fontSize: 18),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Icon(
                Icons.font_download,
                color: Colors.grey,
              ),
              Text(
                ' Cỡ chữ & Font',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Icon(
                Icons.format_paint,
                color: Colors.grey,
              ),
              Text(
                ' Theme',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Icon(
                Icons.volume_up,
                color: Colors.grey,
              ),
              Text(
                ' Giọng đọc',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              ),
              Spacer(),
              Text(
                ' Mặc định',
                style: TextStyle(color: Colors.grey, fontSize: 14),
              ),
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Icon(
                Icons.markunread,
                color: Colors.grey,
              ),
              Text(
                ' Tin địa phương',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              ),
              Spacer(),
              Text(
                ' Chọn địa phương',
                style: TextStyle(color: Colors.grey, fontSize: 14),
              ),
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Icon(
                Icons.settings,
                color: Colors.grey,
              ),
              Text(
                ' Nâng cao',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget buildSanPham() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Text(
                'SẢN PHẨM',
                style: TextStyle(
                    color: Color.fromARGB(255, 11, 167, 91), fontSize: 18),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Text(
                ' Liên hệ',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Text(
                ' Đối tác chính thức',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Text(
                ' Kiểm tra bản mới',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              ),
              Spacer(),
              Text(
                '24.07.02',
                style: TextStyle(
                    color: Color.fromARGB(255, 11, 167, 91), fontSize: 14),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Text(
                ' Điều khoản sử dụng',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Text(
                ' Chính sách bảo mật',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Text(
                ' Bình chọn cho Báo Mới',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Text(
                ' Email góp ý, báo lỗi',
                style: TextStyle(color: Colors.grey, fontSize: 16),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget buildLogOut() {
    return Container(
        padding: EdgeInsets.all(10),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Text(
                    'Đăng xuất',
                    style: TextStyle(
                        color: Color.fromARGB(255, 11, 167, 91), fontSize: 18),
                  )
                ],
              ),
            ]));
  }
}
